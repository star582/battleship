﻿using UnityEngine;
using System.Collections;

public class ShipSlide : MonoBehaviour {

	public ArrayList cannons;
	
	public int type;
	public int id;                  //order in the ShipDB

	public double health;
	public double weight;
	public double cornering;
	public double thrust;
	public double max_speed;
	public double penetrate;

	public bool torpedo;

	public double armor;
	public double price;

	public double reload;   //unused

	public Vector2 position;

	// Use this for initialization
	void Start () {
		cannons = new ArrayList();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void initWithID(int id)
	{
		this.id = id;

		this.type = ShipDB.getTypeByID(id);

		this.health = ShipDB.stat[id, 0];
		this.weight = ShipDB.stat[id, 1];
		this.cornering = ShipDB.stat[id, 2];
		this.thrust = ShipDB.stat[id, 3];
		this.max_speed = ShipDB.stat[id, 4];
		this.penetrate = ShipDB.stat[id, 5];
		this.torpedo = (ShipDB.stat[id, 6] != 0);
		this.armor = ShipDB.stat[id, 7];
		this.price = ShipDB.stat[id, 8];
	}
}
