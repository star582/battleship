﻿using UnityEngine;
using System.Collections;

public class Formation : MonoBehaviour {

	public static Vector2 FirstSlideLocation = new Vector2(248f, -30f);				//Vectors locating slides (right side)
	public static Vector2 SlideLocationDifference = new Vector2(0f, -75f);
	public static Vector2 TabLocationDifference = new Vector2(0f, -110f);

	public static Vector2 LeftFleetLocation = new Vector2(-187f, 224f);				//Vectors locating selected slides (left side)
	public static Vector2 RightFleetLocation = new Vector2(126f, 224f);
	public static Vector2 FleetLocationDifference = new Vector2(0f, -85f);

	public int Money;

	public GameObject slideprefab;
	public GameObject fleetslideprefab;
	public Sprite[] thumbnails;
	public Sprite[] pictures;

	public System.Collections.Generic.List<int> fleet;								//THE LIST containing selected shipIDs

	public int selected = 0;

	// Use this for initialization
	void Start () {
		fleet = new System.Collections.Generic.List<int>();
		Money = 330;

		GameObject Tabs = GameObject.Find("Tabs");
		GameObject MoneyText = GameObject.Find("MoneyText");
		MoneyText.GetComponent<UnityEngine.UI.Text>().text = "$".ToString() + Money.ToString();

		for (int i = 0; i < 4; ++i)													//load ShipDB, make slides
		{
			GameObject Tab = Tabs.transform.GetChild(i).gameObject;
			
			Tab.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = ShipDB.type_name[i];

			if (i == selected)
			{
				Tab.GetComponent<UnityEngine.UI.Image>().color = new Color(200f / 255f, 200f / 255f, 200f / 255f);
			}
			else
			{
				Tab.GetComponent<UnityEngine.UI.Image>().color = Color.white;
			}
			for (int j = 0; j < ShipDB.type_number[i]; ++j)
			{
				GameObject slide = GameObject.Instantiate(slideprefab);
				Vector3 tmp = slide.transform.localScale;
				slide.transform.SetParent(Tab.transform.GetChild(1).transform);
				slide.transform.localScale = tmp;
				slide.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().sprite = this.thumbnails[i];
				slide.GetComponent<Slide>().id = ShipDB.getIDByType(i, j);
				slide.GetComponent<ShipSlide>().initWithID(slide.GetComponent<Slide>().id);
				slide.transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = ShipDB.type_name[i] + j.ToString();
				slide.transform.GetChild(2).GetComponent<UnityEngine.UI.Text>().text = "$".ToString() + slide.GetComponent<ShipSlide>().price.ToString();
					
				slide.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(
					() => {this.ClickSlide(slide.GetComponent<Slide>().id);});
				slide.transform.GetChild(3).GetComponent<UnityEngine.UI.Button>().onClick.AddListener(
					() => {this.ClickAdd(slide.GetComponent<Slide>().id);});

				slide.transform.localPosition = FirstSlideLocation + j * SlideLocationDifference;
				
				if (i == selected)
					slide.SetActive(true);
				else
					slide.SetActive(false);
			}
		}

		GameObject FinishButton = GameObject.Find("FinishButton");
		FinishButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(
			() => { this.ClickFinish(); });
	}

	// Update is called once per frame
	void Update () {
		GameObject MoneyText = GameObject.Find("MoneyText");
		MoneyText.GetComponent<UnityEngine.UI.Text>().text = "$".ToString() + Money.ToString();

		string temp = "";
	}

	void OnMouseDown() {

	}

	public void ClickTab(int input)													//when Tabs(DD, B, CC, CV) are clicked
	{
		GameObject Tabs = GameObject.Find("Tabs");
		
		selected = input;

		for (int i = 0; i < 4; ++i)														//hide unselected slides, show selected slides
		{
			GameObject Tab = Tabs.transform.GetChild(i).gameObject;
			if (i == selected)
			{
				Tab.GetComponent<UnityEngine.UI.Image>().color = new Color(200f / 255f, 200f / 255f, 200f / 255f);

				for (int j = 0; j < 4; ++j)
				{
					GameObject slide = Tab.transform.GetChild(1).GetChild(j).gameObject;
					slide.SetActive(true);
				}
			}
			else
			{
				Tab.GetComponent<UnityEngine.UI.Image>().color = Color.white;

				for (int j = 0; j < 4; ++j)
				{
					GameObject slide = Tab.transform.GetChild(1).GetChild(j).gameObject;
					slide.SetActive(false);
				}
			}
		}
	}

	public void ClickSlide(int id)
	{
		//
		//Show Information on info window
		//
	}

	public void ClickAdd(int id)													//when clicked the '+' button in slides
	{
		Money -= ShipDB.stat[id, 8];														//check whether the player has enough money
		if (Money < 0)																				
		{
			Money += ShipDB.stat[id, 8];
		}
		else																				//add the selection to the fleet, make new fleetslide
		{
			GameObject FleetSlides = GameObject.Find("FleetSlides");
			GameObject FleetSlide = GameObject.Instantiate(fleetslideprefab);
			Vector3 tmp = FleetSlide.transform.localScale;

			FleetSlide.transform.SetParent(FleetSlides.transform);
			FleetSlide.transform.localScale = tmp;
			if (fleet.Count % 2 == 0)
			{
				FleetSlide.transform.localPosition = LeftFleetLocation + fleet.Count / 2 * FleetLocationDifference;
			}
			if (fleet.Count % 2 == 1)
			{
				FleetSlide.transform.localPosition = RightFleetLocation + fleet.Count / 2 * FleetLocationDifference;
			}
			FleetSlide.GetComponent<FleetSlide>().id = id;
			FleetSlide.GetComponent<FleetSlide>().fleetid = fleet.Count;
			FleetSlide.GetComponent<ShipSlide>().initWithID(FleetSlide.GetComponent<FleetSlide>().id);

			FleetSlide.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = ShipDB.type_name[ShipDB.getTypeByID(FleetSlide.GetComponent<ShipSlide>().id)] + ShipDB.getIndexByID(FleetSlide.GetComponent<ShipSlide>().id).ToString();
			FleetSlide.transform.GetChild(1).GetComponent<UnityEngine.UI.Button>().onClick.AddListener(
				() => { this.ClickDelete(FleetSlide.GetComponent<FleetSlide>().fleetid); });

			fleet.Add(id);
		}
	}

	public void ClickDelete(int fleetid)											//when clicked the '-' button in fleetslides
	{
		Debug.Log(fleetid);

		GameObject FleetSlides = GameObject.Find("FleetSlides");

		for(int i = 0; i < fleet.Count; ++i)
		{
			GameObject FleetSlide = FleetSlides.transform.GetChild(i).gameObject;

			if(i == fleetid)
			{
				Destroy(FleetSlide);
			}
			else if(i < fleetid)
			{
				continue;
			}
			else if(i > fleetid)
			{
				FleetSlide.GetComponent<FleetSlide>().fleetid = i-1;
				
				if ((i - 1)% 2 == 0)
				{
					FleetSlide.transform.localPosition = LeftFleetLocation + ((i - 1) / 2) * FleetLocationDifference;
				}
				if ((i - 1) % 2 == 1)
				{
					FleetSlide.transform.localPosition = RightFleetLocation + ((i - 1) / 2) * FleetLocationDifference;
				}
			}
		}

		Money += ShipDB.stat[fleet[fleetid], 8];
		fleet.RemoveAt(fleetid);		
	}

	public void ClickFinish()														//when clicked the formation finish button
	{
		// Formation Scene -> Battle Scene.

		GameObject FleetContainer = GameObject.Find("FleetContainer");
		FleetContainer.GetComponent<FleetContainer>().fleet = fleet;
		DontDestroyOnLoad(FleetContainer);
		Application.LoadLevel(1);
	}
}