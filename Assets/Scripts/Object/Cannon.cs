﻿using UnityEngine;
using System.Collections;

public class Cannon : MonoBehaviour {

	// Use this for initialization
	private bool _mouseState;
	
	/// <summary>
	/// 마우스가 다운된 오브젝트
	/// </summary>
	public Vector3 target;
	/// <summary>
	/// 마우스 좌표
	/// </summary>
	private Vector3 MousePos;

	public float velocity;
	public GameObject ship;
	public System.Collections.Generic.List<Action> actions;

	//시간이 흐를 때 돌려줄 방향
	Quaternion rotation;

	void Start() 
	{
		actions = new System.Collections.Generic.List<Action>();
		this.ship = this.gameObject.transform.parent.gameObject;
		velocity = 1000.0f;
	}

	void Update()
	{
		
	}

	public void Shoot()
	{
		Action shoot = new Action();

		shoot.ship = this.ship;
		shoot.target = this.target;
		shoot.cannon = this.gameObject;
		shoot.type = Action.Type.CannonShot;

		float Vz, Vh, time, reach, distance;
		Vector3 EstShipPosition;
		float bestVz = 99999.0f, bestVh = 99999.0f, bestTime = 99999.0f, bestReach = 99999.0f, bestDistance = 99999.0f;
		Vector3 bestEstShipPosition = new Vector3(0.0f, 0.0f, 0.0f);
		float bestError = float.MaxValue; 
		for (float i = 0; i < Mathf.PI / 4.0f; i += Mathf.PI / 100000.0f)
		{
			Vz = this.velocity * Mathf.Sin(i);
			Vh = this.velocity * Mathf.Cos(i);

			time = (Vz + Mathf.Sqrt(Vz * Vz + 2 * Physics.gravity.magnitude * this.gameObject.transform.position.z)) / Physics.gravity.magnitude;
				//(2 * Vz / Physics.gravity.magnitude);
			reach = time * Vh;

			EstShipPosition = this.gameObject.transform.position + this.ship.GetComponent<Rigidbody>().velocity * time;

			distance = (this.target - EstShipPosition).magnitude;

			float Error = Mathf.Abs(distance - reach);

			if (Error < bestError)
			{
				bestError = Error;
				bestVz = Vz;
				bestVh = Vh;
				bestTime = time;
				bestReach = reach;
				bestDistance = distance;
				bestEstShipPosition = EstShipPosition;
			}
		}

		Vector3 toTarget = -bestEstShipPosition + this.target;

		Debug.Log(bestEstShipPosition);
		GameObject shell = GameObject.Instantiate(GameObject.Find("Level").GetComponent<TestGM>().ShellPrefab);
		shoot.cannonball = shell;
		shoot.time = bestTime;
		shell.GetComponent<Rigidbody>().velocity = toTarget.normalized * bestVh - new Vector3(0.0f, 0.0f, bestVz) + ship.GetComponent<Rigidbody>().velocity;
		shell.transform.localPosition = this.gameObject.transform.position;
		actions.Add(shoot);
		shell.GetComponent<CannonBall>().action = shoot;
	}
	/*
	// Update is called once per frame 
	void Update () 
	{
		//오브젝트 선택
		//마우스가 내려갔는지?
		if ( true == Input.GetMouseButtonDown(0)) 
		{
			//내려갔다.
			
			//타겟을 받아온다.
			target = GetClickedObject();

			//타겟이 나인가?
			if ( target != null && true == target.Equals(gameObject)) 
			{
				//있으면 마우스 정보를 바꾼다.
				_mouseState = true; 
			}
			
		}
		else if ( true == Input.GetMouseButtonUp(0)) 
		{
			//마우스가 올라 갔다.
			//마우스 정보를 바꾼다.
			_mouseState = false; 
		}
		
		
		
		//마우스가 눌렸나?
		//스케일을 조절하여 클릭됬음을 확인한다.
		if (true == _mouseState)
		{
			//눌렸다!
			transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		}
		else
		{
			transform.localScale = new Vector3(1f, 1f, 1f);
		}


		//오브젝트 회전 및 발사
		if( Input.GetMouseButtonUp(1) ) 
		{

			transform.Translate (Vector3.forward * 2.0f * Time.deltaTime);
			//Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);



			//타겟이 나인가?

			if ( target != null && true == target.Equals(gameObject)) 
			{
				//target의 돌려줄 방향을 정한다.
				Vector3 pos = Input.mousePosition;
				Vector3 lookdir = Camera.main.ScreenToWorldPoint(pos);
				Vector3 dir = (lookdir - target.transform.position).normalized;
				rotation = Quaternion.LookRotation(dir);
			}
		}

		//돌려준다.


		
	} 
	
	
	/// <summary>
	/// 마우스가 내려간 오브젝트를 가지고 옵니다.
	/// </summary>
	/// <returns>선택된 오브젝트</returns>
	private GameObject GetClickedObject() 
	{
		//충돌이 감지된 영역
		RaycastHit hit;
		//찾은 오브젝트
		GameObject target = null; 
		
		//마우스 포이트 근처 좌표를 만든다.
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
		
		//마우스 근처에 오브젝트가 있는지 확인
		if( true == (Physics.Raycast(ray.origin, ray.direction * 10, out hit))) 
		{
			//있다!
			
			//있으면 오브젝트를 저장한다.
			target = hit.collider.gameObject; 
		} 
		
		return target; 
	}
	*/
}
