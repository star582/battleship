﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	public ArrayList cannons;
	
	public int type;
	public int id;                  //order in the ShipDB

    public double health_max;
	public double health;
    public int speed = 5;
	public double weight;
	public double cornering;
	public double thrust;
	public double max_speed;
	public double penetrate;

	public bool torpedo;

	public double armor;
	public double price;

	public double reload;   //unused

	public Vector2 position; // unused

	public Vector3 ShipSize;

	public Vector3 CenterDisplacement;

	public ArrayList actions;

	public Action move;

	// Use this for initialization
	void Start () {
		this.initWithID(0);
		move = new Action();
		move.target = new Vector3(1000.0f, 1000.0f, 0.0f);
		this.GetComponent<Rigidbody>().useGravity = false;
		this.GetComponent<Rigidbody>().velocity = -this.transform.right * (float)max_speed;

		for (int i = 0; i < this.transform.childCount; ++i)
		{
			this.transform.GetChild(i).gameObject.AddComponent<Cannon>();
		}
		GameObject tmp = GameObject.Instantiate(this.gameObject);
		tmp.transform.LookAt(transform.position - Vector3.forward, Vector3.down);


		
		ShipSize = tmp.GetComponent<MeshRenderer>().bounds.size;
		Vector3 ShipPosition = tmp.transform.localPosition;
		Vector3 ShipCenter = tmp.GetComponent<MeshRenderer>().bounds.center;
		CenterDisplacement = ShipCenter - ShipPosition;
		CenterDisplacement.z = 0.0f;
		cannons = new ArrayList();
		Destroy(tmp);
		//this.transform.LookAt( transform.position - Vector3.forward , - Vector3.up);

        health_max = health;
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 toTarget = move.target - this.transform.position;
		Vector3 destination = this.GetComponent<Rigidbody>().velocity.normalized * toTarget.magnitude;
		Vector3 diff = toTarget - destination;
		int direction = 0;
		if (Vector3.Cross(this.GetComponent<Rigidbody>().velocity.normalized , toTarget).z > 0) { direction = -1; } else { direction = 1; };

		// May change the mechanism
		// Straight forward movement vs. Rotation around the destination
		if (diff.magnitude > 1.0f)
		{
			if (direction == -1)
			{
				this.GetComponent<Rigidbody>().AddForce(-Vector3.Cross(this.GetComponent<Rigidbody>().velocity.normalized, Vector3.forward) * (float)cornering * 2.0f * Time.deltaTime, ForceMode.Acceleration);
				this.transform.LookAt(transform.position - Vector3.forward, new Vector3(-this.GetComponent<Rigidbody>().velocity.y, this.GetComponent<Rigidbody>().velocity.x, 0.0f));
			}
			else
			{
				this.GetComponent<Rigidbody>().AddForce(Vector3.Cross(this.GetComponent<Rigidbody>().velocity.normalized, Vector3.forward) * (float)cornering * 2.0f * Time.deltaTime, ForceMode.Acceleration);
				this.transform.LookAt(transform.position - Vector3.forward, new Vector3(-this.GetComponent<Rigidbody>().velocity.y, this.GetComponent<Rigidbody>().velocity.x, 0.0f));
			}
		}


        Debug.Log(this.GetComponent<Rigidbody>().velocity);
	}

	public void initWithID(int id)
	{
		this.id = id;

		this.type = ShipDB.getTypeByID(id);

		this.health = ShipDB.stat[id, 0];
		this.weight = ShipDB.stat[id, 1];
		this.cornering = ShipDB.stat[id, 2];
		this.thrust = ShipDB.stat[id, 3];
		this.max_speed = ShipDB.stat[id, 4];
		this.penetrate = ShipDB.stat[id, 5];
		this.torpedo = (ShipDB.stat[id, 6] != 0);
		this.armor = ShipDB.stat[id, 7];
		this.price = ShipDB.stat[id, 8];
	}

	void OnMouseDown()
	{
		GameObject cursor = GameObject.Find("SelectCursor");

		cursor.transform.GetChild(0).GetComponent<Renderer>().enabled = true;
		cursor.transform.GetChild(1).GetComponent<Renderer>().enabled = true;
		cursor.transform.GetChild(2).GetComponent<Renderer>().enabled = true;
		cursor.transform.GetChild(3).GetComponent<Renderer>().enabled = true;
		cursor.transform.LookAt(cursor.transform.position - Vector3.forward, this.transform.up);
		cursor.transform.SetParent(this.gameObject.transform);

		cursor.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
		cursor.transform.localScale = new Vector3(1.0f, 1.0f, 0.0f);

		cursor.transform.GetChild(0).transform.localPosition = CenterDisplacement + new Vector3(-ShipSize.x / 2, ShipSize.y / 2, 0.0f);
		cursor.transform.GetChild(1).transform.localPosition = CenterDisplacement + new Vector3(-ShipSize.x / 2, -ShipSize.y / 2, 0.0f);
		cursor.transform.GetChild(2).transform.localPosition = CenterDisplacement + new Vector3(ShipSize.x / 2, ShipSize.y / 2, 0.0f);
		cursor.transform.GetChild(3).transform.localPosition = CenterDisplacement + new Vector3(ShipSize.x / 2, -ShipSize.y / 2, 0.0f);		
	}
}