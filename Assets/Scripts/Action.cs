﻿using UnityEngine;
using System.Collections;

public class Action {

	public enum Type { Move, CannonShot, TorpedoShot };

	public Type type;

	public Vector3 target;

	public float time;
	
	public bool completed = false;

	public GameObject ship;

	public GameObject cannon;

	public GameObject cannonball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
}
