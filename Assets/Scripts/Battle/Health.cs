﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    public Ship ship;

	// Use this for initialization
	void Start () {
	}   
	
	// Update is called once per frame
	void Update () {
        this.transform.GetChild(1).localScale = new Vector3((float)ship.health / (float)ship.health_max, 1, 0);
        // if (ship.health > 0) ship.health -= 0.5;
	}   
}
