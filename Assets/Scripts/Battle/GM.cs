﻿using UnityEngine;
using System.Collections;

public class GM : MonoBehaviour {

	// Game Management : main control of variables.

	public GameObject[] ships;

	Vector3 pos1, pos2;


	/* state : each ship's stat in game.
	 * same order as 'stats & Formation.elements'
	 */
	public static int[,] state = new int[6, 8];



	// Use this for initialization
	void Start () {

		/*
		string temp = "";
		for (int i=0; i < Formation.element_pos; i++) {
			temp = temp + Formation.elements[i].ToString() + " ";
		}
		GetComponent<TextMesh> ().text = temp;
		*/
		
		pos1 = new Vector3 ((float)-2.5, 0, 0);
		pos2 = new Vector3 ((float)-4.5, (float)-2.5, 0);

		// state initialization.
		//for (int i=0; i < Formation.element_pos; i++) {
		//	create_ships(i+1, Formation.elements[i]);
		//    for (int j = 0; j < ShipDB.init_stat_num; j++)
		//    {
		//		state[i, j] = ShipDB.init_stat[Formation.elements[i], j];
		//	}
		//}



	}
	
	// Update is called once per frame
	void Update () {
	
	}



	void create_ships(int i, int n) {
		GameObject obj = GameObject.Instantiate (ships[n]) as GameObject;
		obj.name = "ship " + i.ToString();
		obj.transform.SetParent (GameObject.Find ("1 - battle").transform);
		obj.transform.localPosition = pos1;
		obj.transform.localScale = new Vector3 ((float)0.7, (float)	0.7, 1);
		
		GameObject obj2 = GameObject.Instantiate (ships[n]) as GameObject;
		obj2.name = "ship_manage " + i.ToString();
		obj2.transform.SetParent (GameObject.Find ("2 - manage").transform);
		obj2.transform.localPosition = pos2;


		pos1 = pos1 + new Vector3 (1, 0, 0);
		if (pos2.y == (float)-2.5)
			pos2 = pos2 + new Vector3 (0, (float)-1, 0);
		else
			pos2 = pos2 + new Vector3 (3, (float)1, 0);

	}



}
