﻿using UnityEngine;
using System.Collections;

public class Speed : MonoBehaviour {

    public Ship ship;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        switch (ship.speed){
            case 1:
                this.transform.GetChild(1).localPosition = new Vector3(27.5F, -22.5F, 0);
                break;
            case 2:
                this.transform.GetChild(1).localPosition = new Vector3(38.6F, -22.5F, 0);
                break;
            case 3:
                this.transform.GetChild(1).localPosition = new Vector3(50F, -22.5F, 0);
                break;
            case 4:
                this.transform.GetChild(1).localPosition = new Vector3(62F, -22.5F, 0);
                break;
            case 5:
                this.transform.GetChild(1).localPosition = new Vector3(73F, -22.5F, 0);
                break;
            default:
                break;
        }
        
        //this.transform.GetChild(1).localScale = new Vector3((float)ship.health / (float)ship.health_max, 1, 0);
	}
}
