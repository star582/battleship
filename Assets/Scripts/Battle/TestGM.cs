﻿using UnityEngine;
using System.Collections;

public class TestGM : MonoBehaviour {

	public static float turn_length = 10.0f;

	// Battle Scene Management

	public System.Collections.Generic.List<int> fleet;

	public GameObject[] ShipPrefab;

    public GameObject[] ShipPrefabUI;
    Vector3 ship_pos = new Vector3((float)-15, 0, 0);
    Vector3 UI_ship = new Vector3((float)-350, (float)-150, 0);
    Vector3 UI_health = new Vector3((float)50, (float)-50, 0);
    Vector3 UI_speed = new Vector3(0, 0, 0);


	public GameObject ShellPrefab;
	Vector3 pos1, pos2;
	
	public Ship ShipSelect; // Current selected ship
	
	// Use this for initialization
	void Start()
	{
		timeStop();

		GameObject cursor = GameObject.Find("SelectCursor");
		cursor.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
		cursor.transform.GetChild(1).GetComponent<Renderer>().enabled = false;
		cursor.transform.GetChild(2).GetComponent<Renderer>().enabled = false;
		cursor.transform.GetChild(3).GetComponent<Renderer>().enabled = false;
		cursor.transform.SetParent(this.gameObject.transform);

		GameObject target = GameObject.Find("target");
		target.GetComponent<Renderer>().enabled = false;

		this.fleet = GameObject.Find("FleetContainer").GetComponent<FleetContainer>().fleet;		// Load fleet

		Physics.gravity = new Vector3(0.0f, 0.0f, 9.8f);

		//
		// Make ships from "fleet"
		//


        
		for (int i = 0; i < fleet.Count; i++)
		{
			create_ships(fleet[i], i);
		}


		GameObject TurnEndButton = GameObject.Find("TurnEndButton");
		TurnEndButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(
			() => { StartCoroutine(this.startTurn()); });
	}

	IEnumerator startTurn()
	{
		if (isTimeStopped())
		{
			this.ClickTurnEnd();
			yield return new WaitForSeconds(turn_length);
			this.timeStop();
		}
	}
	// Update is called once per frame
	public void Update (){
		
	}

	public void create_ships(int id, int i)
	{
		int a = ShipDB.getTypeByID(id);

		GameObject obj = GameObject.Instantiate(ShipPrefab[a]) as GameObject;
		obj.GetComponent<Ship>().initWithID(id);
		obj.name = "ship " + (i+1).ToString();
		obj.transform.SetParent(GameObject.Find("1 - battle").transform);
        obj.transform.localPosition = ship_pos;
		
		GameObject obj2 = GameObject.Instantiate(ShipPrefabUI[0]) as GameObject;
		obj2.name = "shipUI " + (i+1).ToString();
		obj2.transform.SetParent(GameObject.Find("ShipUI").transform);
        obj2.transform.localPosition = UI_ship;

        GameObject obj3 = GameObject.Instantiate(ShipPrefabUI[1]) as GameObject;
        obj3.name = "health " + (i + 1).ToString();
        obj3.transform.SetParent(GameObject.Find(obj2.name).transform);
        obj3.transform.localPosition = UI_health;
        obj3.GetComponent<Health>().ship = obj.GetComponent<Ship>();
        
        GameObject obj4 = GameObject.Instantiate(ShipPrefabUI[2]) as GameObject;
        obj4.name = "speed " + (i + 1).ToString();
        obj4.transform.SetParent(GameObject.Find(obj2.name).transform);
        obj4.transform.localPosition = UI_speed;
        obj4.GetComponent<Speed>().ship = obj.GetComponent<Ship>();
        


        ship_pos = ship_pos + new Vector3(8, 0, 0);
        UI_ship = UI_ship + new Vector3(100, 0, 0);

	}

	public void timeStop()			//stop the time
	{
		Time.timeScale = 0.0f;
	}

	public void timeResume()		//resume the timescale to 1.0
	{
		Time.timeScale = 1.0f;
	}

	public bool isTimeStopped()
	{
		if (Time.timeScale == 0.0f)
		{
			return true;
		}
		return false;
	}

	public void ClickTurnEnd()														//when clicked the formation finish button
	{
		this.timeResume();
	}

	public void OnMouseDown()
	{
		GameObject cursor = GameObject.Find("SelectCursor");

		cursor.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
		cursor.transform.GetChild(1).GetComponent<Renderer>().enabled = false;
		cursor.transform.GetChild(2).GetComponent<Renderer>().enabled = false;
		cursor.transform.GetChild(3).GetComponent<Renderer>().enabled = false;

		cursor.transform.SetParent(this.gameObject.transform);

		Cannon cannon = GameObject.Find("BBwhole 1").transform.GetChild(0).GetComponent<Cannon>();
		cannon.target = new Vector3(10000.0f, 10000.0f, 0.0f);
		cannon.Shoot();

	}

	IEnumerator OnMouseOver()
    {
        GameObject cursor = GameObject.Find("SelectCursor");
        ShipSelect = cursor.transform.parent.GetComponent<Ship>();


		if (ShipSelect != null && isTimeStopped())
        {
            if (true == Input.GetMouseButtonDown(1))
            {	
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				Plane plane = new Plane(Vector3.forward, Vector3.zero);
				float dist = 0.0f;
				plane.Raycast(ray, out dist);
				Vector3 position = ray.GetPoint(dist);

				GameObject target = GameObject.Find("target");

                ShipSelect.move.target = position;
				target.transform.localPosition = position;
				target.transform.GetComponent<Renderer>().enabled = true;
				for (int i= 0; i < 0.5f / Time.fixedDeltaTime; i++ )
				{
					yield return null;
				}
				target.transform.GetComponent<Renderer>().enabled = false;
			}

            else if (Input.anyKey)
            {
                int speed_past = ShipSelect.speed;

                if (Input.GetKey("1"))
                {
                    ShipSelect.speed = 1;
                    float delta_speed = (float)1 / (float)speed_past;
                    ShipSelect.GetComponent<Rigidbody>().velocity = ShipSelect.GetComponent<Rigidbody>().velocity * delta_speed;
                }
                else if (Input.GetKey("2"))
                {
                    ShipSelect.speed = 2;
                    float delta_speed = (float)2 / (float)speed_past;
                    ShipSelect.GetComponent<Rigidbody>().velocity = ShipSelect.GetComponent<Rigidbody>().velocity * delta_speed;
                }
                else if (Input.GetKey("3"))
                {
                    ShipSelect.speed = 3;
                    float delta_speed = (float)3 / (float)speed_past;
                    ShipSelect.GetComponent<Rigidbody>().velocity = ShipSelect.GetComponent<Rigidbody>().velocity * delta_speed;
                }
                else if (Input.GetKey("4"))
                {
                    ShipSelect.speed = 4;
                    float delta_speed = (float)4 / (float)speed_past;
                    ShipSelect.GetComponent<Rigidbody>().velocity = ShipSelect.GetComponent<Rigidbody>().velocity * delta_speed;
                }
                else if (Input.GetKey("5"))
                {
                    ShipSelect.speed = 5;
                    float delta_speed = (float)5 / (float)speed_past;
                    ShipSelect.GetComponent<Rigidbody>().velocity = ShipSelect.GetComponent<Rigidbody>().velocity * delta_speed;
                }
            }
		}
	}
}