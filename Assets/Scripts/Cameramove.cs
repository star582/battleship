﻿using UnityEngine;
using System.Collections;

public class Cameramove : MonoBehaviour
{

	float speed = 0.05f; //좌우 이동 속도

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		MoveCamera(Vector3.right * speed * this.gameObject.GetComponent<Camera>().orthographicSize * Input.GetAxisRaw("Horizontal"));
		MoveCamera(Vector3.up * speed * this.gameObject.GetComponent<Camera>().orthographicSize * Input.GetAxisRaw("Vertical"));
		Zoom(Input.GetAxis("Mouse ScrollWheel"));
	}

	void Zoom(float scroll)
	{
		this.gameObject.GetComponent<Camera>().orthographicSize *= Mathf.Pow(0.8f, scroll);
		GameObject.Find("target").transform.localScale *= Mathf.Pow(0.8f, scroll);
		GameObject.Find("SelectCursor").transform.GetChild(0).localScale *= Mathf.Pow(0.8f, scroll);
		GameObject.Find("SelectCursor").transform.GetChild(1).localScale *= Mathf.Pow(0.8f, scroll);
		GameObject.Find("SelectCursor").transform.GetChild(2).localScale *= Mathf.Pow(0.8f, scroll);
		GameObject.Find("SelectCursor").transform.GetChild(3).localScale *= Mathf.Pow(0.8f, scroll);
	}

	void MoveCamera(Vector3 displacement)
	{
		this.transform.Translate(displacement);
		/*
		float amtMove = speed ;
		float key_h = Input.GetAxisRaw("Horizontal");
		transform.Translate(Vector3.right * amtMove * key_h);
		float key_v = Input.GetAxisRaw("Vertical");
		transform.Translate(Vector3.up * amtMove * key_v);
		*/
	}
}
