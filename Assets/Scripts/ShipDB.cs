﻿using UnityEngine;
using System.Collections;

public class ShipDB : MonoBehaviour {


	/* stats : each ship's initial stat. "MAIN POINT of balance."
	 * order
	 * 1) DD - C -  BB - CV
	 * 2) {hp, weight, cornering, momentum, max_speed, penentrate, torpedo_OX, armor, price}
	 * 
	 */


	public static int init_stat_num = 8;
	
	public static string[] type_name = new string[4] { "DD", "C", "BB", "CV" };
	public static int[] type_number = new int[4] {4,4,4,4};

	public static int[,] stat = new int[16, 9]
	{   {100, 300, 100, 100, 40, 150, 1, 130, 50},
		{100, 330, 100, 130, 37, 170, 1, 150, 50},
		{100, 330, 100, 120, 32, 130, 1, 150, 50},
		{100, 400, 95, 100, 40, 180, 1, 200, 50},
		{200, 500, 100, 100, 35, 300, 1, 250, 50},
		{180, 700, 100, 100, 33, 400, 1, 230, 50},
		{190, 600, 100, 100, 35, 350, 1, 230, 50},
		{130, 550, 100, 100, 31, 330, 1, 270, 50},
		{300, 900, 100, 100, 30, 450, 0, 400, 50},
		{400, 1200, 100, 100, 25, 550, 0, 470, 50},
		{330, 880, 100, 100, 27, 470, 0, 380, 50},
		{350, 95000, 100, 100, 29, 500, 0, 350, 50},
		{100, 300, 100, 100, 40, 150, 1, 130, 50},
		{200, 500, 100, 100, 35, 300, 1, 250, 50},
		{180, 700, 100, 100, 33, 400, 1, 230, 50},
		{300, 900, 100, 100, 30, 450, 0, 400, 50}};

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static int getTypeByID(int id)
	{
		int i;
		for(i=0;i<4;++i)
		{
			id -= type_number[i];
			if (id < 0)
				break;
		}

		return i;
	}

	public static int getIndexByID(int id)
	{
		int i;
		for(i=0;i<4;++i)
		{
			id -= type_number[i];
			if (id < 0)
				break;
		}

		return id + type_number[i];
	}

	public static int getIDByType(int type, int number)
	{
		int id = 0;
		for (int i = 0; i < (int)type; ++i)
		{
			id += ShipDB.type_number[i];
		}
		id += number;

		return id;
	}
}
